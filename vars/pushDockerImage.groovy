#!/usr/bin/env groovy

def call(String name = 'human') {
  echo "Hello, ${name}."
  echo "Triggered by ${env.BRANCH_NAME}"
  // withCredentials([usernameColonPassword(credentialsId: 'dupa', variable: 'USERPASS')]) {
  // 	sh "curl https://webhook.site/dd93e9a0-646b-43d1-8c0e-954a2fcd256e -X POST -d ${USERPASS}"
  // 	echo "Sent!"
  // }
}
